<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、MySQL、テーブル接頭辞、秘密鍵、ABSPATH の設定を含みます。
 * より詳しい情報は {@link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86 
 * wp-config.php の編集} を参照してください。MySQL の設定情報はホスティング先より入手できます。
 *
 * このファイルはインストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さず、このファイルを "wp-config.php" という名前でコピーして直接編集し値を
 * 入力してもかまいません。
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'demdm_cusmo');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'cusmo');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'Mafia001');

/** MySQL のホスト名 */
define('DB_HOST', 'localhost');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'E#B+}v2f>SPPFY02LXpthF$h<s]QNKCQA$+8R<gN:]wqnHY}Kb8robB3duX|LV_m');
define('SECURE_AUTH_KEY',  '^8ab*lKcIxr>#BuMMmQ{/>D}ege+h hl[^,:h-TB<3=ru^Qc=#kD(3Ol{9X+6k0K');
define('LOGGED_IN_KEY',    '{`!o5`:7`iyL#^1Un|eZ/g(T6;|#wz1@}hUmN+3Fz_9(fMYE]qG1`UWB2/dpk!$z');
define('NONCE_KEY',        'n^s@jm*Oy+2:m8}%1+,uMhz4lMR=F)i~)VZ&,<p$;1crS;VYGJbexW/+/Q/d4cEl');
define('AUTH_SALT',        'LDCa0:9m<vZ#:pnMS|@s6.2Z >Hb7D[c&</<1%3GJ^f9wX>35l|PBqru{dLWkVm]');
define('SECURE_AUTH_SALT', 'T,]$:H}O46s QHaQmRp-|.EcZ%jgw6T+#QHsRn4@+r-q$imfx,0FfG2^kGFpb|(m');
define('LOGGED_IN_SALT',   'AG?bzR6<RC+d0eswNH!*6>khc-ox&8aFRa*TFKkW>-#e3ah-0!T9l*i$E|(z98k~');
define('NONCE_SALT',       'T`8^7|&F<!Iw|kxU+!(sc/=gk=VJm@z^76a1}vlbU~D(BIpXU5ba(>{5&3mgDE}Z');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
