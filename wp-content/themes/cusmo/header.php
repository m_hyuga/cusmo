<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<!doctype html>
<html lang="ja-JP">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
	<meta charset="UTF-8">
	<meta content="" name="copyright">
	<meta content="" name="description">
	<meta name="format-detection" content="telephone=no" />
	<meta name="viewport" content="user-scalable=yes,width=1400" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="http://www.css-reset.com/css/meyer.css" />
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/common.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/common.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/skrollr-master/src/skrollr.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/jquery.SmoothScroll/jquery.smoothScroll.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/jquery.tile.min.js"></script>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/js/colorbox/colorbox.css" />
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/colorbox/jquery.colorbox-min.js"></script>
	<?php if (is_post_type_archive('intro')): ?><link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/intro.css" /><?php endif; ?>
	<?php if (is_category('news') || in_category('news')): ?><link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/news.css" /><?php endif; ?>
	<?php if (is_post_type_archive('event') || get_post_type() === 'event' ): ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/event.css" />
	<script src="http://maps.google.com/maps/api/js?v=3&sensor=false" type="text/javascript" charset="UTF-8"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/google_map.js"></script>
	<?php endif; ?>
	<?php if (is_page('shop') || is_404()): ?><link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/shop.css" /><?php endif; ?>
	<?php if (is_page('inquiry')): ?><link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/inquiry.css" /><?php endif; ?>
	<?php if (is_page('area')): ?><link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/area.css" /><?php endif; ?>
	<?php if (is_page('concept')): ?><link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/concept.css" /><?php endif; ?>
	<?php if (is_page('privacy')): ?><link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/privacy.css" /><?php endif; ?>
	<?php if (is_page('product') || is_parent_slug() === 'product' ): ?><link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/product.css" /><?php endif; ?>
	<?php if (is_post_type_archive('works') || get_post_type() === 'works'): ?><link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/works.css" /><?php endif; ?>
	<?php if (is_page('use') || is_parent_slug() === 'use' ): ?><link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/use.css" /><?php endif; ?>
	<?php if (is_page('about')): ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/about.css" />
	<script src="http://maps.google.com/maps/api/js?v=3&sensor=false" type="text/javascript" charset="UTF-8"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/google_map.js"></script>
	<?php endif; ?>
	<!--[if lt IE 9]>
		<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
<script type="text/javascript">
	$(function(){
		//userAgentでFOOTERのcss変更
	  var agent = navigator.userAgent;
	  if(agent.search(/iPhone/) != -1 || agent.search(/iPad/) != -1 || agent.search(/iPod/) != -1 || agent.search(/Android/) != -1){
	    $('#footer').css({'font-size':'10px'});
	    $('#footer .adr').css({'font-size':'10px'});
	    $('.foo_contact img').css({'width':'130px','height':'129px'});
	  }
	});
</script>
</head>
<body>

	<div class="btn_fixed">
		<a href="<?php bloginfo('url'); ?>/inquiry/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/btn_request.jpg" alt="お問い合わせ・資料請求"></a>
		<a href="<?php bloginfo('url'); ?>/event/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/btn_inq.jpg" alt="イベント情報"></a>
	</div>

	<div id="header">
		<div class="head_abs">
			<h1><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/head_logo.png" width="381" height="33" alt="CUSMO"></a></h1>
			<div class="menu">
				<ul class="cf">
					<li><a href="<?php bloginfo('url'); ?>/concept/">CUSMOとは</a></li>
					<li id="cs1" class="so"><a href="<?php bloginfo('url'); ?>/use/">CUSMOで土地活用</a></li>
					<li id="cs2" class="so"><a href="<?php bloginfo('url'); ?>/product/">CUSMOの商品紹介</a></li>
					<li id="cs3" class="so"><a href="<?php bloginfo('url'); ?>/area/">建築可能エリア</a></li>
					<li><a href="<?php bloginfo('url'); ?>/intro/">加盟店紹介</a></li>
					<li><a href="<?php bloginfo('url'); ?>/shop/">加盟店募集</a></li>
					<li id="cs4" class="las"><a href="javascript:void(0);">施工事例</a></li>
				</ul>
			</div>
		</div>
