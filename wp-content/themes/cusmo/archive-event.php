<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
		<div class="h2_ttl"><h2><img src="<?php bloginfo('template_url'); ?>/common/images/event/ttl_event.jpg" width="" height="" alt="イベント"></h2></div>
	</div>

	<div id="event" class="cf">
		<p class="archive">
		<select name="archive-dropdown" onChange='document.location.href=this.options[this.selectedIndex].value;'>
		<option value=""><?php echo attribute_escape(__('年別アーカイブ')); ?></option>
		<?php wp_get_archives('type=yearly&post_type=event&format=option'); ?>
		</select></p>

		<ul class="event_list cf">
		<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<li>
				<a href="<?php the_permalink(); ?>">
					<p class="thumb_img" style="background-image: url('<?php	$image_id = SCF::get('photo_01');
						$image = wp_get_attachment_image_src($image_id, 'full');
						if (empty($image_id)) {}else{echo $image[0];};
					?>')"></p>
					<h4><?php $val = nl2br(get_post_meta($post->ID, 'txt_date', true)); if (!empty($val)) {	echo $val;} ?></h4>
					<p><?php if(mb_strlen($post->post_title)>30) { $title= mb_substr($post->post_title,0,30) ; echo $title. ･･･ ;} else {echo $post->post_title;}?></p>
					<div class="btnmore"><img src="<?php bloginfo('template_url'); ?>/common/images/top/btn_more.jpg" width="82" height="17" alt=""></div>
				</a>
			</li>
		<?php endwhile; endif; ?>
		</ul>
		<div class="pager cf">
			<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
		</div>
	</div>
<?php get_footer(); ?>
