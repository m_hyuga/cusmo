<?php
/**
* The template for displaying 404 pages (not found)
*
* @package WordPress
* @subpackage Twenty_Fifteen
* @since Twenty Fifteen 1.0
*/

get_header(); ?>
</div>
<div style="width:900px; margin:0 auto;">
申し訳ありません、ページが見つかりません。<br><br>
<a href="<?php bloginfo('url'); ?>" style="color:#000;">トップページに戻る。</a>
</div>

<br><br><br><br>
<?php get_footer(); ?>
