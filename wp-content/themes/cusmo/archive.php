<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<style>
#header .menu.kari ul {
	width: 900px!important;
}
.menu.kari ul li {
	width: auto!important;
	padding: 10px 0!important;
}
.menu.kari ul li a {
	padding: 0 65px!important;
	border-right: 1px solid #fff!important;
}
.menu.kari ul li:first-child a {
	border-left: 1px solid #fff!important;
}
#footer .foo_contact {
	margin-top: -60px;
}
#footer .inner {
	padding-bottom: 30px;
}
</style>
		<div class="h2_ttl"><h2><img src="<?php bloginfo('template_url'); ?>/common/images/news/ttl_news.jpg" width="" height="" alt="ニュース"></h2></div>
	</div>

	<div id="news" class="cf">
		<ul class="news_list">
		<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<li class="cf">
				<div class="fll thumb_img" style="background-image: url('<?php if ( has_post_thumbnail() ) { // アイキャッチの有無のチェック
									echo wp_get_attachment_url( get_post_thumbnail_id() );
									}else{
									echo bloginfo("template_url");
									echo '/common/images/news/dummy.jpg';} ?>')"></div>
				<div class="flr">
					<div class="newstitle cf"><div class="deta"><span><?php the_time('Y'); ?></span><?php the_time('m/d'); ?></div><h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3><div class="deta_btn"><img src="<?php bloginfo('template_url'); ?>/common/images/intro/menu_mark.jpg" alt=""></div></div>
					<p><?php
					$theContentForPreSingle = mb_substr(strip_tags($post-> post_content), 0, 250);
					$theContentForPreSingle = preg_replace('/\［caption(.+?)\/caption\］/','',$theContentForPreSingle);
					$theContentForPreSingle = preg_replace('/\［gallery(.+?)\］/','',$theContentForPreSingle);
					echo $theContentForPreSingle;
					if(mb_strlen($theContentForPreSingle) >= 250){echo '...';};
					?></p>
				</div>
			</li>
		<?php endwhile; endif; ?>
		</ul>
		<div class="pager cf">
			<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
		</div>
	</div>
<?php get_footer(); ?>
