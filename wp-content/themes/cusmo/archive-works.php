<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<div class="h2_ttl"><h2><img src="<?php bloginfo('template_url'); ?>/common/images/works/ttl_works.jpg" width="291" height="63" alt="施工事例"></h2></div>
</div>

<div id="works">

<h3>CUSMOで設計・施工させていただきましたお客様宅の一例をご紹介いたします。</h3>
<ul class="category cf">
	<li><a href="<?php bloginfo('url'); ?>/works/works_cat/roof/" class="over"><img src="<?php bloginfo('template_url'); ?>/common/images/works/btn_roof.jpg" alt="CUSMO ROOF"></a></li>
	<li><a href="<?php bloginfo('url'); ?>/works/works_cat/arch/" class="over"><img src="<?php bloginfo('template_url'); ?>/common/images/works/btn_arch.jpg" alt="CUSMO ARCH"></a></li>
</ul>

<ul class="works_list cf">
		<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
		<li>
			<a href="<?php the_permalink(); ?>">
				<p class="thumb_img" style="background-image: url('<?php	$image_id = SCF::get('img_thumb');
						$image = wp_get_attachment_image_src($image_id, 'full');
						if (empty($image_id)) {}else{echo $image[0];};
					?>')"></p>
				<p class="cateTxt">CUSMO ARCH</p>
				<p class="nameTxt"><?php the_title(); ?></p>
			</a>
		</li>
		<?php endwhile; endif; ?>
</ul>

		<div class="pager cf">
			<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
		</div>

</div>
<?php get_footer(); ?>
