<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
		<div class="h2_ttl"><h2><img src="<?php bloginfo('template_url'); ?>/common/images/intro/ttl_intro.jpg" width="" height="" alt="加盟店募集"></h2></div>
	</div>
		<style>
		#header .menu.kari ul {
			width: 900px!important;
		}
		.menu.kari ul li {
			width: auto!important;
			padding: 10px 0!important;
		}
		.menu.kari ul li a {
			padding: 0 65px!important;
			border-right: 1px solid #fff!important;
		}
		.menu.kari ul li:first-child a {
			border-left: 1px solid #fff!important;
		}
		#footer .foo_contact {
			margin-top: -60px;
		}
		#footer .inner {
			padding-bottom: 30px;
		}
		</style>

	<div id="intro" class="cf">
		<ul class="pagelink fll">
			<li><a href="#toyama"><img src="<?php bloginfo('template_url'); ?>/common/images/intro/menu_mark.jpg" width="13" height="13" alt="">富山県内</a></li>
		</ul>
		<div class="flr">
			<h3 id="toyama" class="plttl">富山県内</h3>
			<ul class="int_list">
				<?php query_posts( Array(
					'post_type' => array('intro'),
					'showposts' => -1,
					'intro_cat' => 'toyama'
					));
				if (have_posts()) : while (have_posts()) : the_post(); ?>
				<li class="cf">
					<div class="thumb_img" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>')"></div>
					<div class="fll">
						<h4><?php the_title(); ?></h4>
						<p><?php $val =  scf::get('txt-address');
								if (empty($val)) {
										echo '';
									} else {
										echo $val;
									}
								echo '<br>';	
								$val =  scf::get('txt-tel');
								if (empty($val)) {
										echo '';
									} else {
										echo 'TEL.'.$val.'　';
									}
								$val =  scf::get('txt-fax');
								if (empty($val)) {
										echo '';
									} else {
										echo 'FAX.'.$val.'　';
									} ?></p>
						<?php $val =  scf::get('txt-url');
						if (empty($val)) {
							echo '';
						} else {
							echo '<a href="';
							echo $val;
							echo '" target="_blank"><img src="';
							echo bloginfo("template_url");
							echo '/common/images/intro/btn_hp.jpg" width="213" height="27" alt="ホームページはこちらから"></a>';
						} ?>
						
					</div>
				</li>
				<?php endwhile; endif; ?>
			</ul>
		</div>
	</div>
<?php get_footer(); ?>
