<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
	<script type="text/javascript">
	$(function(){
		$('#thm li').click(function(){
			var Index = $('#thm li').index(this);//クリックされた要素が何番目かを変数に代入
			$('#pic li').css({'display':'none'});//一度拡大画像は全て非表示
			$('#pic li').eq(Index).css({'display':'block'});//クリックされた要素と同じ順番の拡大画像を表示
		});
	});
	</script>
		<div class="h2_ttl"><h2><img src="<?php bloginfo('template_url'); ?>/common/images/works/ttl_works.jpg" width="291" height="63" alt="施工事例"></h2></div>
	</div>

	<div id="works_deta">
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>

		<h2><?php $terms = get_the_terms( $post->ID, 'works_cat' );
if ( $terms && ! is_wp_error( $terms ) ) : 
	$draught_links = array();
	foreach ( $terms as $term ) {
		$draught_links[] = $term->name;
	}
	$on_draught = join( "  ", $draught_links );
	echo $on_draught .'　';
	endif; ?><?php the_title(); ?></h2>

	 <div class="cf">
			<ul id="pic">
				<?php
				$repeat_group = scf::get('list_content');
				$cat_cnt = 0;
				foreach ( $repeat_group as $field_name => $field_value ) :
					?>
				<?php 
				echo '<li';
				if ($cat_cnt === 0) {echo ' class="first"';}
				echo '><img src="';
				$val =  $field_value["img_list"];
					if (empty($val)) {
						echo '';
					} else {
					$image = wp_get_attachment_image_src($val, 'full');echo $image[0];
					}
				echo '" alt="">';
				$val = $field_value['txt_list']; if (empty($val)) {
						echo '';
					} else {
					echo '<p>'.$val.'</p>';
					}
				echo '</li>';
				 ?>
				<?php $cat_cnt++; endforeach; ?>
			</ul>
			<div class="flr">
				<ul id="thm" class="cf">
				<?php
				$repeat_group = scf::get('list_content');
				$cat_cnt = 0;
				foreach ( $repeat_group as $field_name => $field_value ) :
					?>
					<li><p class="thumb_img" style="background-image: url('<?php 
				$val =  $field_value["img_list"];
					if (empty($val)) {
						echo '';
					} else {
					$image = wp_get_attachment_image_src($val, 'full');echo $image[0];
					}
				 ?>')"></p></li>
				<?php endforeach; ?>
				</ul>
				<p class="exp"><img src="<?php bloginfo('template_url'); ?>/common/images/works/exp.jpg" alt="">画像をクリックすると拡大します</p>
			</div>
		</div>
<?php endwhile; endif; ?>

		<div class="inqBtn">
			<a href="<?php bloginfo('url'); ?>/inquiry/"><img src="<?php bloginfo('template_url'); ?>/common/images/works/btn_inq.jpg" alt="資料請求はこちら"></a>
		</div>

	</div>

<?php get_footer(); ?>
