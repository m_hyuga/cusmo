<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
	<meta charset="UTF-8">
	<meta content="" name="copyright">
	<meta content="" name="description">
	<meta name="format-detection" content="telephone=no" />
	<meta name="viewport" content="user-scalable=yes,width=1400" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
	<link rel="stylesheet" href="http://www.css-reset.com/css/meyer.css" />
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/common.css" />
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/top.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/common.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/skrollr-master/src/skrollr.js"></script>
	<!--[if lt IE 9]>
		<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
	<![endif]-->
<script type="text/javascript">
$(function(){
	//userAgentでFOOTERのcss変更
  var agent = navigator.userAgent;
  if(agent.search(/iPhone/) != -1 || agent.search(/iPad/) != -1 || agent.search(/iPod/) != -1 || agent.search(/Android/) != -1){
    $('#footer').css({'font-size':'10px'});
    $('#footer .adr').css({'font-size':'10px'});
    $('.foo_contact img').css({'width':'130px','height':'129px'});
    //header
    $('#header').css({'height':'860px'});
  }
});
</script>
</head>
<body id="top">

	<div class="btn_fixed">
		<a href="<?php bloginfo('url'); ?>/inquiry/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/btn_request.jpg" alt="お問い合わせ・資料請求"></a>
		<a href="<?php bloginfo('url'); ?>/event/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/btn_inq.jpg" alt="イベント情報"></a>
	</div>

	<div id="header">
		<h1><img src="<?php bloginfo('template_url'); ?>/common/images/top/top_arrow.png" width="42" height="20" alt=""></h1>
		<div id="menu">
			<ul class="cf">
				<li><a href="<?php bloginfo('url'); ?>/concept/">CUSMOとは</a></li>
				<li class="cs1"><a href="<?php bloginfo('url'); ?>/use/">CUSMOで土地活用</a></li>
				<li class="cs2"><a href="<?php bloginfo('url'); ?>/product/">CUSMOの商品紹介</a></li>
				<li class="cs3"><a href="<?php bloginfo('url'); ?>/area/">建築可能エリア</a></li>
				<li><a href="<?php bloginfo('url'); ?>/intro/">加盟店紹介</a></li>
				<li><a href="<?php bloginfo('url'); ?>/shop/">加盟店募集</a></li>
				<li class="cs4"><a href="javascript:void(0);">施工事例</a></li>
			</ul>
		</div>
	</div><!-- /header -->

	<div class="concept">
		<h3><img src="<?php bloginfo('template_url'); ?>/common/images/top/ttl_concept.jpg" width="311" height="55" alt="CONCEPT CUSMOとは"></h3>
		<h4 class="ttl_concept">戸建賃貸住宅の常識を変える。<br>それがism-Style CUSMOです。</h4>
		<p class="txt_concept">ism-Style CUSMOは、建てる人と暮らす人の「こだわり」を大切にした戸建賃貸住宅。<br>仕様も設計も、設備もデザインも、これまでとは違う品質の高さが特長です。<br>自分で「住む」。人に「貸す」。<br>どちらにも最適な高品質住宅CUSMOなら<br>あなたらしい住まいで、あなたらしいライフスタイルを実現できます。</p>
		<div class="btn_more"><a href="<?php bloginfo('url'); ?>/concept/"><img src="<?php bloginfo('template_url'); ?>/common/images/top/btn_more.jpg" class="over" width="82" height="17" alt="more"></a></div>
	</div>

	<div class="bene_pro">
		<div class="cf">
			<div class="fll"><a href="<?php bloginfo('url'); ?>/use/"><img src="<?php bloginfo('template_url'); ?>/common/images/top/btn_benefits.jpg" alt="カスモで土地利用" class="over"></a></div>
			<div class="fll"><a href="<?php bloginfo('url'); ?>/product/"><img src="<?php bloginfo('template_url'); ?>/common/images/top/btn_product.jpg" alt="カスモの商品紹介" class="over"></a></div>
		</div>
	</div>

	<div class="shop">
		<h3><a href="<?php bloginfo('url'); ?>/shop/"><img src="<?php bloginfo('template_url'); ?>/common/images/top/ttl_store.jpg" alt="CUSMOの加盟店になりませんか。" class="over"></a></h3>
	</div>

	<div class="event">
		<h3><img src="<?php bloginfo('template_url'); ?>/common/images/top/ttl_events.jpg" width="283" height="58" alt="EVENTS イベント情報"></h3>
		<p class="btn_event"><a href="<?php bloginfo('url'); ?>/event/"><img src="<?php bloginfo('template_url'); ?>/common/images/top/btn_event.jpg" alt="イベント一覧へ" class="over"></a></p>
		<ul class="cf">
		<?php query_posts( Array(
				'post_type' => array('event'),
				'showposts' => 4
				));
			if (have_posts()) : while (have_posts()) : the_post(); ?>
			<li>
				<a href="<?php the_permalink(); ?>">
					<p class="thumb_img" style="background-image: url('<?php	$image_id = SCF::get('photo_01');
						$image = wp_get_attachment_image_src($image_id, 'full');
						if (empty($image_id)) {}else{echo $image[0];};
					?>')"></p>
					<h4><?php $val = nl2br(get_post_meta($post->ID, 'txt_date', true)); if (!empty($val)) {	echo $val;} ?></h4>
					<p><?php if(mb_strlen($post->post_title)>30) { $title= mb_substr($post->post_title,0,30) ; echo $title. ･･･ ;} else {echo $post->post_title;}?></p>
					<div class="btnmore"><img src="<?php bloginfo('template_url'); ?>/common/images/top/btn_more.jpg" width="82" height="17" alt=""></div>
				</a>
			</li>
		<?php endwhile; ?>
		<?php endif; ?>
		</ul>
	</div>

	<div class="news">
		<h3><img src="<?php bloginfo('template_url'); ?>/common/images/top/ttl_news.jpg" width="283" height="58" alt=""></h3>
		<div class="info cf">
			<?php
			query_posts( Array('showposts' => 3));
			if (have_posts()) : while (have_posts()) : the_post(); ?>
			<dl class="cf">
				<dt><?php the_time('Y.m.d'); ?></dt>
				<dd><a href="<?php the_permalink(); ?>"><?php if(mb_strlen($post->post_title)>30) { $title= mb_substr($post->post_title,0,30) ; echo $title. ･･･ ;} else {echo $post->post_title;}?></a></dd>
			</dl>
			<?php endwhile; endif; ?>
			<div class="news_btn cf">
				<img src="<?php bloginfo('template_url'); ?>/common/images/top/news_up.jpg" class="btn_01" width="22" height="21" alt="">
				<img src="<?php bloginfo('template_url'); ?>/common/images/top/news_bottom.jpg" class="btn_02" width="22" height="21" alt="">
				<a href="<?php bloginfo('url'); ?>/news/"><img src="<?php bloginfo('template_url'); ?>/common/images/top/news_list.jpg" class="btn_03 over" width="72" height="16" alt=""></a>
			</div>
		</div>
	</div>

	<div class="para" data-bottom-top="background-position:50% 0%" data-0-end="background-position:50% 100%"></div>

	<div class="contact">
		<h3><img src="<?php bloginfo('template_url'); ?>/common/images/top/ttl_contact.jpg" width="306" height="57" alt=""></h3>
		<div class="btn_contact"><a href="<?php bloginfo('url'); ?>/inquiry/"><img src="<?php bloginfo('template_url'); ?>/common/images/top/btn_contact.jpg" class="over" width="506" height="76" alt=""></a></div>
		<p>ご質問など、お気軽にお問い合わせください。</p>
	</div>

<?php get_footer(); ?>
