$(function(){

	//発火
	if($('#top').size()){
		//TOPページのみに効かせる処理
		var agent = navigator.userAgent;
		if(agent.search(/iPhone/) != -1 || agent.search(/iPad/) != -1 || agent.search(/iPod/) != -1 || agent.search(/Android/) != -1){
			//
		} else {
			topMain();
		}
		topMenu();
		pageTop();
		indexMenuFix();
		topNews();
		pageSideBtn();
	} else {
		pageTop();
		pageMenu();
		pagemenuLink();
		lineupLink();
		eventSlide();
		pageSideBtn();
		conceptTile();
	}

	//TOPのメインビジュアル
	function topMain(){
		$(window).on('load resize', function(){
			WH = $(window).height();
			$('#header').height(WH);
		});
		$('h1 img').click(function(){
			$('html,body').animate({scrollTop:WH});
		});
		$(window).load(function(){
			setTimeout(function(){
				var s = skrollr.init();
			}, 1000);
		});
	}

	//overカミングスーン処理
	function topMenu() {
//		$('#menu ul li.cs3 a').hover(function(){
//			$(this).text('COMING SOON');
//		}, function(){
//			$(this).text('建築可能エリア');
//		});
		$('#menu ul li.cs4 a').hover(function(){
			$(this).text('COMING SOON');
		}, function(){
			$(this).text('施工事例');
		});
//		$('.bene_pro .fll').hover(function(){
//			$(this).append('<span class="over_soon"><img src="common/images/top/over_soon.png" alt="" /></span>');
//		}, function(){
//			$('.over_soon').remove();
//		});
	}

	function pageMenu() {
//		$('#header #cs3 a').hover(function(){
//			$(this).text('COMING SOON');
//		}, function(){
//			$(this).text('建築可能エリア');
//		});
		$('#header #cs4 a').hover(function(){
			$(this).text('COMING SOON');
		}, function(){
			$(this).text('施工事例');
		});
	}

	function pageTop() {
		$(window).on("load scroll", function(){
			var scrTop = $(window).scrollTop();
			if(scrTop > 300) {
				$('#pagetop').fadeIn();
			} else {
				$('#pagetop').fadeOut();
			}
		});
		$('#pagetop').click(function(){
			$('html,body').animate({scrollTop: 0}, 500);
			return false;
		});
	}

	//スクロールでついてくるメニュー
	function indexMenuFix() {
		$(window).load(function(){
			var headH = $('#header').height();
			var menuH = $('#menu').height();
			var fnHeight = headH-menuH;
			var scr = $(window).scrollTop();
			if(scr >= fnHeight) {
				$('#menu').css({
					'position':'fixed',
					'top':0,
					'bottom':'auto'
				});
			} else {
				$('#menu').css({
					'position':'absolute',
					'top':'auto',
					'bottom':0
				});
			}
			$(window).scroll(function(){
				var scr = $(window).scrollTop();
				if(scr >= fnHeight) {
					$('#menu').css({
						'position':'fixed',
						'top':0,
						'bottom':'auto'
					});
				} else {
					$('#menu').css({
						'position':'absolute',
						'top':'auto',
						'bottom':0
					});
				}
			});
		});
	}

	function pagemenuLink() {
		$('.pagemenu li a').click(function(){
			var aId = $(this).attr('id');
			if(aId == 'kit') {
				var kit_offset = $('#kitchen').offset();
				$('body, html').animate({ scrollTop: kit_offset.top-120 }, 500);
			} else if(aId == 'bat') {
				var bat_offset = $('#bathroom').offset();
				$('body, html').animate({ scrollTop: bat_offset.top-120 }, 500);
			} else if(aId == 'uti') {
				var uti_offset = $('#utility').offset();
				$('body, html').animate({ scrollTop: uti_offset.top-120 }, 500);
			} else if(aId == 'toi') {
				var toi_offset = $('#toilet').offset();
				$('body, html').animate({ scrollTop: toi_offset.top-120 }, 500);
			}
		});
	}

	function lineupLink() {
		$('.lineup_link li a').click(function(){
			var aId = $(this).attr('id');
			if(aId == 'ty') {
				var kit_offset = $('#type').offset();
				$('body, html').animate({ scrollTop: kit_offset.top-120 }, 500);
			} else if(aId == 'de') {
				var bat_offset = $('#design').offset();
				$('body, html').animate({ scrollTop: bat_offset.top-120 }, 500);
			} else if(aId == 'op') {
				var uti_offset = $('#option').offset();
				$('body, html').animate({ scrollTop: uti_offset.top-120 }, 500);
			}
		});
		$('.each_title li a').click(function() {
			var aId = $(this).attr('id');
			if(aId == 'cr') {
				var kit_offset = $('#cus_roof').offset();
				$('body, html').animate({ scrollTop: kit_offset.top-120 }, 500);
			} else if(aId == 'ca') {
				var bat_offset = $('#cus_arch').offset();
				$('body, html').animate({ scrollTop: bat_offset.top-120 }, 500);
			}
		});
		$('.each_title li a').click(function() {
			var aId = $(this).attr('id');
			if(aId == 'cre') {
				var kit_offset = $('#cus_roof_ex').offset();
				$('body, html').animate({ scrollTop: kit_offset.top-120 }, 500);
			} else if(aId == 'cae') {
				var bat_offset = $('#cus_arch_ex').offset();
				$('body, html').animate({ scrollTop: bat_offset.top-120 }, 500);
			} else if(aId == 'ci') {
				var bat_offset = $('#cus_int').offset();
				$('body, html').animate({ scrollTop: bat_offset.top-120 }, 500);
			}
		});
		$('.page_nav li a').click(function() {
			var aId = $(this).attr('id');
			if(aId == 'con1') {
				var kit_offset = $('#con_1').offset();
				$('body, html').animate({ scrollTop: kit_offset.top-120 }, 500);
			} else if(aId == 'con2') {
				var bat_offset = $('#con_2').offset();
				$('body, html').animate({ scrollTop: bat_offset.top-120 }, 500);
			} else if(aId == 'con3') {
				var bat_offset = $('#con_3').offset();
				$('body, html').animate({ scrollTop: bat_offset.top-120 }, 500);
			} else if(aId == 'con4') {
				var bat_offset = $('#con_4').offset();
				$('body, html').animate({ scrollTop: bat_offset.top-120 }, 500);
			} else if(aId == 'con5') {
				var bat_offset = $('#con_5').offset();
				$('body, html').animate({ scrollTop: bat_offset.top-120 }, 500);
			} else if(aId == 'con6') {
				var bat_offset = $('#con_6').offset();
				$('body, html').animate({ scrollTop: bat_offset.top-120 }, 500);
			} else if(aId == 'con7') {
				var bat_offset = $('#con_7').offset();
				$('body, html').animate({ scrollTop: bat_offset.top-120 }, 500);
			}
		});
	}

	function eventSlide() {
		$('.thm li').click(function(){
			var liIndex = $('.thm li').index(this);  //クリックされた要素が何番目か
			$('.slide li').css({'display':'none'});  //一度全スライドを非表示
			$('.slide li').eq(liIndex).css({'display':'block'});  //クリックされたサムネと同じ番号のスライド表示
		});
	}

	function topNews() {
		$('.info dl:first').addClass('select'); //最初の要素にクラス付与
		$('.info dl:first').css({'display':'block'}); //最初の要素にクラス付与
		$('.news_btn .btn_01').click(function(){ //ボタンクリックしたら
			if($('.select').prev('dl').hasClass('cf')) { //.selectの前に要素がある場合のみ
				$('.select').fadeOut(function(){ //　現select要素をフェードアウトさせて、
					$('.select').removeClass('select');  //クラスも外す
					$(this).prev().addClass('select');  //現selectフェードが終わったら前の要素にクラスを付与し、
					$(this).prev().fadeIn();   //フェードインさせる。
				});
			} else {
				//一番上の状態で上を選んだ場合、一番下にループする
				$('.select').fadeOut(function(){ //　現select要素をフェードアウトさせて、
					$('.select').removeClass('select');  //クラスも外す
					$('.info dl:last').addClass('select');  //現selectフェードが終わったら前の要素にクラスを付与し、
					$('.info dl:last').fadeIn();   //フェードインさせる。
				});
			}
		});$('.news_btn .btn_02').click(function(){
			if($('.select').next('dl').hasClass('cf')) {
				$('.select').fadeOut(function(){
					$('.select').removeClass('select');
					$(this).next().addClass('select');
					$(this).next().fadeIn();
				});
			} else {
				//一番下の状態で下を選んだ場合、一番上にループする
				$('.select').fadeOut(function(){ //　現select要素をフェードアウトさせて、
					$('.select').removeClass('select');  //クラスも外す
					$('.info dl:first').addClass('select');  //現selectフェードが終わったら前の要素にクラスを付与し、
					$('.info dl:first').fadeIn();   //フェードインさせる。
				});
			}
		});
	}

	function pageSideBtn() {
		$('.btn_fixed img').hover(function(){
			$(this).stop(true,false).animate({'right':'0px'},200);
		}, function(){
			$(this).stop(true,false).animate({'right':'-5px'},200);
		});
	}

	function conceptTile() {
		$(window).load(function() {
			$('#concept_list > li').tile(2);
		});
	}

});