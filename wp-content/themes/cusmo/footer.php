<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
	<div id="footer">
		<div class="inner cf">
			<h2><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/foot_logo.png" width="208" height="54" alt=""></a></h2>
			<div class="fll">
				<h3>CUSMO事業部 <span>[南陽（株）事業本部内]</span></h3>
				<p class="adr">&#12306;933-0005　富山県高岡市能町南2-52　TEL.0766-27-7300　FAX.0766-22-8808</p>
				<ul class="sitemap cf">
					<li><a href="<?php bloginfo('url'); ?>/concept/">CUSMOとは</a></li>
					<li><a href="<?php bloginfo('url'); ?>/use/">CUSMOで土地活用</a></li>
					<li><a href="<?php bloginfo('url'); ?>/product/">CUSMOの商品紹介</a></li>
					<li><a href="<?php bloginfo('url'); ?>/area/">建築可能エリア</a></li>
				</ul>
				<ul class="sitemap cf">
					<li><a href="<?php bloginfo('url'); ?>/intro/">加盟店紹介</a></li>
					<li><a href="<?php bloginfo('url'); ?>/shop/">加盟店募集</a></li>
					<li><a href="javascript:void(0);">施工事例</a></li>
					<li><a href="<?php bloginfo('url'); ?>/about/">会社概要</a></li>
					<li><a href="<?php bloginfo('url'); ?>/privacy/">プライバシーポリシー</a></li>
				</ul>
			</div>
			<div class="foo_contact fll cf">
				<a href="<?php bloginfo('url'); ?>/inquiry/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/foot_req.png" class="over" width="149" height="148" alt=""></a>
				<a href="<?php bloginfo('url'); ?>/event/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/foot_contact.png" class="over" width="149" height="148" alt=""></a>
			</div>
			<div id="pagetop"><img src="<?php bloginfo('template_url'); ?>/common/images/common/pagetop.png" class="over" width="48" height="22" alt=""></div>
		</div>
		<p class="copy">Copyright &copy; 2015 CUSMO All rights reserved.</p>
	</div>

<?php wp_footer(); ?>

</body>
</html>
