<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
		<div class="h2_ttl"><h2><img src="<?php bloginfo('template_url'); ?>/common/images/event/ttl_event.jpg" width="" height="" alt="ニュース"></h2></div>
	</div>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<?php $val =  scf::get('txt_map');
			if (empty($val)) {	echo '';} else { ?>
<script>
	$(document).ready(function(){
	googlemap_init('google_map', '<?php echo $val;?>');
	});
</script>
<?php ;} ?>
	<div id="event" class="detail">
		<h3 class="plttl"><?php the_title(); ?></h3>
		<div class="cf">
			<div class="fll">
					<?php	
					$check =  scf::get('check_chirashi');
					
					if ($check == 1) {
					echo '<h4>&#12296; 写真 &#12297;</h4>';
					} else {
					echo '<h4>&#12296; 外観・内観 &#12297;</h4>';
					}
					
					echo '<ul class="slide">';
					$image_id = SCF::get('photo_01');
					$image = wp_get_attachment_image_src($image_id, 'full');
					if (empty($image_id)) {}else{echo '<li><img src="'.$image[0].'" alt=""></li>';};
					$image_id = SCF::get('photo_02');
					$image = wp_get_attachment_image_src($image_id, 'full');
					if (empty($image_id)) {}else{echo '<li class="hide"><img src="'.$image[0].'" alt=""></li>';};
					$image_id = SCF::get('photo_03');
					$image = wp_get_attachment_image_src($image_id, 'full');
					if (empty($image_id)) {}else{echo '<li class="hide"><img src="'.$image[0].'" alt=""></li>';};
					$image_id = SCF::get('photo_04');
					$image = wp_get_attachment_image_src($image_id, 'full');
					if (empty($image_id)) {}else{echo '<li class="hide"><img src="'.$image[0].'" alt=""></li>';};
					echo '</ul>';
					
					if ($check == 1) {} else {
					echo '<ul class="thm cf">';
					$image_id = SCF::get('photo_01');
					$image = wp_get_attachment_image_src($image_id, 'full');
					if (empty($image_id)) {}else{echo '<li><img src="'.$image[0].'" alt=""></li>';};
					$image_id = SCF::get('photo_02');
					$image = wp_get_attachment_image_src($image_id, 'full');
					if (empty($image_id)) {}else{echo '<li><img src="'.$image[0].'" alt=""></li>';};
					$image_id = SCF::get('photo_03');
					$image = wp_get_attachment_image_src($image_id, 'full');
					if (empty($image_id)) {}else{echo '<li><img src="'.$image[0].'" alt=""></li>';};
					$image_id = SCF::get('photo_04');
					$image = wp_get_attachment_image_src($image_id, 'full');
					if (empty($image_id)) {}else{echo '<li><img src="'.$image[0].'" alt=""></li>';};
					echo '</ul>';
					}
					?>
				
			</div>
			<div class="flr">
				<dl class="res cf">
					<dt><?php $val =  scf::get('txt_type');
										if (empty($val)) {	echo '&nbsp;';} else {	echo $val;} ?></dt>
					<dd><?php $val =  scf::get('txt_modelname');
										if (empty($val)) {	echo '&nbsp;';} else {	echo $val;} ?></dd>
				</dl>
				<dl class="desc cf">
				<?php $val = nl2br(get_post_meta($post->ID, 'txt_date', true));
							if (empty($val)) {	echo '';} else {
									echo '<dt>&#12296;開&ensp;催&ensp;日&#12297;</dt>';
									echo '<dd>'.$val.'</dd>';}
							$val = nl2br(get_post_meta($post->ID, 'txt_times', true));
							if (empty($val)) {	echo '';} else {
									echo '<dt>&#12296;時&emsp;&emsp;間&#12297;</dt>';
									echo '<dd>'.$val.'</dd>';}
							$val = nl2br(get_post_meta($post->ID, 'txt_venue', true));
							if (empty($val)) {	echo '';} else {
									echo '<dt>&#12296;会&emsp;&emsp;場&#12297;</dt>';
									echo '<dd>'.$val.'</dd>';}
							$val = nl2br(get_post_meta($post->ID, 'txt_price', true));
							if (empty($val)) {	echo '';} else {
									echo '<dt>&#12296;本体価格&#12297;</dt>';
									echo '<dd><span>'.$val.'</span>万円(税別)〜</dd>';}
							$val = nl2br(get_post_meta($post->ID, 'txt_detail', true));
							if (empty($val)) {	echo '';} else {
									echo '<dt>&#12296;概&emsp;&emsp;要&#12297;</dt>';
									echo '<dd>'.$val.'</dd>';}
							$val = nl2br(get_post_meta($post->ID, 'txt_moshikomi', true));
							if (empty($val)) {	echo '';} else {
									echo '<dt class="dt_moshikomi">&#12296;お申込方法&#12297;</dt>';
									echo '<dd class="dd_moshikomi">'.$val.'</dd>';}
									
							$val =  scf::get('check_chirashi');
							if ($val == 1) {	
									$file_id = SCF::get('file_chirashi');
									$file = wp_get_attachment_url($file_id);
									if (empty($file_id)) {}else{
										echo '<dd class="dd_chirashi"><a href="'.$file.'" target="_blank">イベントのチラシのダウンロードはこちら</a></dd>';}
							}
							
							
				?>
				</dl>
			</div>
		</div>
		<br>
		<?php $val =  scf::get('txt_map');
					if (empty($val)) {	echo '<br><br><br>';} else {
							echo '<h4 class="ttl_map">&#12296; マップ &#12297;</h4>';
							echo '<div id="google_map" style="width: 1000px; height: 370px"></div>';}
		?>
		
<?php endwhile; endif; ?>
		<div class="ttl_form"><h5>ご来場予約フォーム</h5></div>
<?php echo do_shortcode("[mwform_formkey key='116']"); ?>
		<div class="nomail">
			<h3>メールが届かない方へ</h3>
			<p>お客様が迷惑メール対策等で、ドメイン指定受信を設定されている場合に、メールが正しく届かないことがございます。<br>【infocusmo@nanyo.com】からのメールを受信できるように設定してください。</p>
			<p class="note">※個人情報の取り扱いについては、プライバシーポリシーをご覧ください。</p>
		</div>
	</div>
<?php get_footer(); ?>
