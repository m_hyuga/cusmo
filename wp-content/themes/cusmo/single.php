<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<script>
$(function(){$(".colorbox").colorbox();});
</script>
<style>
#header .menu.kari ul {	width: 900px!important;}
.menu.kari ul li {
	width: auto!important;
	padding: 10px 0!important;
}
.menu.kari ul li a {
	padding: 0 65px!important;
	border-right: 1px solid #fff!important;
}
.menu.kari ul li:first-child a {	border-left: 1px solid #fff!important;}
#footer .foo_contact {	margin-top: -60px;}
#footer .inner {	padding-bottom: 30px;}
</style>
		<div class="h2_ttl"><h2><img src="<?php bloginfo('template_url'); ?>/common/images/news/ttl_news.jpg" width="" height="" alt="ニュース"></h2></div>
	</div>

<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
	<div id="news_deta">
		<div class="newstitle cf">
			<div class="deta"><span><?php the_time('Y'); ?></span><?php the_time('m/d'); ?></div>
			<h3><?php the_title(); ?></h3>
		</div>
		<div class="cf">
			<div class="deta_txt">
			<?php the_content(); ?>
			</div>
			<div class="deta_img"><?php if ( has_post_thumbnail() ) { // アイキャッチの有無のチェック
									echo '<img src="';
									echo wp_get_attachment_url( get_post_thumbnail_id() );
									echo '" alt="">';
									} ?></div>
		</div>
<?php endwhile; endif; ?>
		<p class="back_list"><a href="<?php bloginfo('url'); ?>/news/">一覧へ</a></p>
		<ul class="pager cf">
		<li class="prev"><?php 
		if (get_next_post()){
			next_post_link('%link','<span>&lsaquo;&lsaquo;</span>%title');
		} ?></li>
		<li class="next"><?php 
		if (get_previous_post()){
				previous_post_link('%link','%title<span>&rsaquo;&rsaquo;</span>');
		} ?></li>
		</ul>
	</div>
<?php get_footer(); ?>
