$(function(){

	//発火
	if($('#top').size()){
		//TOPページのみに効かせる処理
		topMain();
		topMenu();
		pageTop();
	} else {
		pageTop();
		pageMenu();
	}

	//TOPのメインビジュアル
	function topMain(){
		$(window).on('load resize', function(){
			WH = $(window).height();
			$('#header').height(WH);
		});
		$('h1 img').click(function(){
			$('html,body').animate({scrollTop:WH});
		});
	}

	//overカミングスーン処理
	function topMenu() {
		$('#menu ul li.cs1 a').hover(function(){
			$(this).text('COMING SOON');
		}, function(){
			$(this).text('CUSMOで土地活用');
		});
		$('#menu ul li.cs2 a').hover(function(){
			$(this).text('COMING SOON');
		}, function(){
			$(this).text('CUSMOの商品紹介');
		});
		$('#menu ul li.cs3 a').hover(function(){
			$(this).text('COMING SOON');
		}, function(){
			$(this).text('建築可能エリア');
		});
		$('.bene_pro .fll').hover(function(){
			$(this).append('<span class="over_soon"><img src="common/images/top/over_soon.png" alt="" /></span>');
		}, function(){
			$('.over_soon').remove();
		});
	}

	function pageMenu() {
		$('#header #cs1 a').hover(function(){
			$(this).text('COMING SOON');
		}, function(){
			$(this).text('CUSMOで土地活用');
		});
		$('#header #cs2 a').hover(function(){
			$(this).text('COMING SOON');
		}, function(){
			$(this).text('CUSMOの商品紹介');
		});
		$('#header #cs3 a').hover(function(){
			$(this).text('COMING SOON');
		}, function(){
			$(this).text('建築可能エリア');
		});
	}

	function pageTop() {
		$('#pagetop').click(function(){
			$('html,body').animate({scrollTop: 0}, 500);
			return false;
		});
	}

});